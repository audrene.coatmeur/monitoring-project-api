<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211102082737 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE alerte (id INT AUTO_INCREMENT NOT NULL, site_id INT NOT NULL, type VARCHAR(255) NOT NULL, date VARCHAR(255) NOT NULL, is_qualified TINYINT(1) NOT NULL, is_healthy TINYINT(1) NOT NULL, comment VARCHAR(255) DEFAULT NULL, INDEX IDX_3AE753AF6BD1646 (site_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE component (id INT AUTO_INCREMENT NOT NULL, machine_id INT DEFAULT NULL, state VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_49FEA157F6B75B26 (machine_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE machine (id INT AUTO_INCREMENT NOT NULL, sote_id INT NOT NULL, state VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, image_url VARCHAR(255) NOT NULL, INDEX IDX_1505DF8420E4F55B (sote_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE site (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, name VARCHAR(255) NOT NULL, image_url VARCHAR(255) DEFAULT NULL, INDEX IDX_694309E4A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE alerte ADD CONSTRAINT FK_3AE753AF6BD1646 FOREIGN KEY (site_id) REFERENCES site (id)');
        $this->addSql('ALTER TABLE component ADD CONSTRAINT FK_49FEA157F6B75B26 FOREIGN KEY (machine_id) REFERENCES machine (id)');
        $this->addSql('ALTER TABLE machine ADD CONSTRAINT FK_1505DF8420E4F55B FOREIGN KEY (sote_id) REFERENCES site (id)');
        $this->addSql('ALTER TABLE site ADD CONSTRAINT FK_694309E4A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE component DROP FOREIGN KEY FK_49FEA157F6B75B26');
        $this->addSql('ALTER TABLE alerte DROP FOREIGN KEY FK_3AE753AF6BD1646');
        $this->addSql('ALTER TABLE machine DROP FOREIGN KEY FK_1505DF8420E4F55B');
        $this->addSql('DROP TABLE alerte');
        $this->addSql('DROP TABLE component');
        $this->addSql('DROP TABLE machine');
        $this->addSql('DROP TABLE site');
    }
}
