<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211102083957 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE machine DROP FOREIGN KEY FK_1505DF8420E4F55B');
        $this->addSql('DROP INDEX IDX_1505DF8420E4F55B ON machine');
        $this->addSql('ALTER TABLE machine CHANGE sote_id site_id INT NOT NULL');
        $this->addSql('ALTER TABLE machine ADD CONSTRAINT FK_1505DF84F6BD1646 FOREIGN KEY (site_id) REFERENCES site (id)');
        $this->addSql('CREATE INDEX IDX_1505DF84F6BD1646 ON machine (site_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE machine DROP FOREIGN KEY FK_1505DF84F6BD1646');
        $this->addSql('DROP INDEX IDX_1505DF84F6BD1646 ON machine');
        $this->addSql('ALTER TABLE machine CHANGE site_id sote_id INT NOT NULL');
        $this->addSql('ALTER TABLE machine ADD CONSTRAINT FK_1505DF8420E4F55B FOREIGN KEY (sote_id) REFERENCES site (id)');
        $this->addSql('CREATE INDEX IDX_1505DF8420E4F55B ON machine (sote_id)');
    }
}
