<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211207132011 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE alerte ADD machine_id INT NOT NULL, ADD component_id INT NOT NULL, ADD user_id INT NOT NULL');
        $this->addSql('ALTER TABLE alerte ADD CONSTRAINT FK_3AE753AF6B75B26 FOREIGN KEY (machine_id) REFERENCES machine (id)');
        $this->addSql('ALTER TABLE alerte ADD CONSTRAINT FK_3AE753AE2ABAFFF FOREIGN KEY (component_id) REFERENCES component (id)');
        $this->addSql('ALTER TABLE alerte ADD CONSTRAINT FK_3AE753AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_3AE753AF6B75B26 ON alerte (machine_id)');
        $this->addSql('CREATE INDEX IDX_3AE753AE2ABAFFF ON alerte (component_id)');
        $this->addSql('CREATE INDEX IDX_3AE753AA76ED395 ON alerte (user_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE alerte DROP FOREIGN KEY FK_3AE753AF6B75B26');
        $this->addSql('ALTER TABLE alerte DROP FOREIGN KEY FK_3AE753AE2ABAFFF');
        $this->addSql('ALTER TABLE alerte DROP FOREIGN KEY FK_3AE753AA76ED395');
        $this->addSql('DROP INDEX IDX_3AE753AF6B75B26 ON alerte');
        $this->addSql('DROP INDEX IDX_3AE753AE2ABAFFF ON alerte');
        $this->addSql('DROP INDEX IDX_3AE753AA76ED395 ON alerte');
        $this->addSql('ALTER TABLE alerte DROP machine_id, DROP component_id, DROP user_id');
    }
}
