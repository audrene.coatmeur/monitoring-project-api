<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\AlerteRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiSubresource;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=AlerteRepository::class)
 */
class Alerte
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"sites_read", "user:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"sites_read", "user:read"})
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"sites_read", "user:read"})
     */
    private $date;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"sites_read", "user:read"})
     */
    private $isQualified;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"sites_read", "user:read"})
     */
    private $isHealthy;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"sites_read", "user:read"})
     */
    private $comment;

    /**
     * @ORM\ManyToOne(targetEntity=Site::class, inversedBy="alertes")
     * @ORM\JoinColumn(nullable=false)
     * @MaxDepth(1)
     */
    private $site;

    /**
     * @ORM\ManyToOne(targetEntity=Machine::class, inversedBy="alertes")
     * @ORM\JoinColumn(nullable=false)
     * @ApiSubresource
     * @Groups("user:read")
     * @MaxDepth(1)
     */
    private $machine;

    /**
     * @ORM\ManyToOne(targetEntity=Component::class, inversedBy="alertes")
     * @ORM\JoinColumn(nullable=false)
     * @ApiSubresource
     * @Groups("user:read")
     * @MaxDepth(1)
     */
    private $component;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="alertes")
     * @ORM\JoinColumn(nullable=false)
     * @MaxDepth(1)
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(string $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getIsQualified(): ?bool
    {
        return $this->isQualified;
    }

    public function setIsQualified(bool $isQualified): self
    {
        $this->isQualified = $isQualified;

        return $this;
    }

    public function getIsHealthy(): ?bool
    {
        return $this->isHealthy;
    }

    public function setIsHealthy(bool $isHealthy): self
    {
        $this->isHealthy = $isHealthy;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getSite(): ?Site
    {
        return $this->site;
    }

    public function setSite(?Site $site): self
    {
        $this->site = $site;

        return $this;
    }

    public function getMachine(): ?Machine
    {
        return $this->machine;
    }

    public function setMachine(?Machine $machine): self
    {
        $this->machine = $machine;

        return $this;
    }

    public function getComponent(): ?Component
    {
        return $this->component;
    }

    public function setComponent(?Component $component): self
    {
        $this->component = $component;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
